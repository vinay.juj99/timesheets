// external dependencies
import express 																															from "express";
import bodyParser 																													from "body-parser";
import mongoose 																														from "mongoose";
import dotenv																																from "dotenv";
dotenv.config(); // for reading from .env file
import cors                                                                 from 'cors';
// middlewares
import { delete_validator, register_validator, timesheet_validator } 				from "./middlewares/index.js";
import { forgot_password_otp_validator, login_validator, forgot_validator }	from "./middlewares/index.js";
import { register_db_validator,  } 																					from "./middlewares/index.js";
import { jwt_token_validator_employee, jwt_token_validator_for_hr} 					from "./middlewares/index.js";
import { jwt_token_validator_for_admin } 																		from "./middlewares/index.js";

// handlers
import { delete_handler, empdetails_handler , login_handler } 							from "./pathhandlers/index.js";
import { timesheet_details_handler , register_handler, time_sheet_handler } from "./pathhandlers/index.js";
import { forgot_password_handler, forgot_handler } 													from "./pathhandlers/index.js";

const app = express();
app.use(bodyParser.json());                           //Returns middleware that only parses json
app.use(bodyParser.urlencoded({ extended: true }));   //Returns middleware that only parses urlencoded bodies
app.use(cors({origin: "*"}))
app.post(		"/create_profile", 			register_validator, 						register_db_validator,		register_handler);
app.post(		"/login", 							login_validator, 																					login_handler);
app.post(		"/forgot_password_otp",	forgot_password_otp_validator, 														forgot_handler);
app.put(		"/forgot_password", 		forgot_validator, 																				forgot_password_handler);

app.get(		"/empdetails", 					jwt_token_validator_for_hr, 															empdetails_handler);
app.post(		"/timesheet", 					jwt_token_validator_employee, 	timesheet_validator, 			time_sheet_handler );
app.get(		"/timesheet_details", 	jwt_token_validator_for_hr, 															timesheet_details_handler);
app.delete(	"/delete/:emp_id", 			jwt_token_validator_for_admin, 	delete_validator, 				delete_handler);

//listnening to server
mongoose.connect(process.env.MONGO_URL)
  .then(() => {
    app.listen(process.env.BACKEND_PORT, () => {
      console.log("Server running sucessfully and listening on port " + process.env.BACKEND_PORT);
    });
  })
  .catch((err) => {
    console.log("Unable to connect to MongoDB. Didnt start server! " + err);
  });