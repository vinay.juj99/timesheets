// import mongoose
import mongoose from "mongoose";

/**
 * Schema for creating UserModel
 */
 const UserSchema = new mongoose.Schema({
  emp_id: 								{ type: Number, 	required: true, unique: true },
  email: 									{ type: String, 	required: true, unique: true },
  mobile_no: 							{ type: Number, 	required: true, unique: true },
  username: 							{ type: String, 	required: true },

  password: 							{ type: String, 	required: true },
  role: 									{ type: String, 	default: "Employee" },
  security_answer: 				{ type: String, 	required: true },
  password_reset_secret:	{ type: String, 	required: true },
  otp: 										{ type: Number },
	is_active: 							{ type: Boolean, 	required: true, default: true }
});
const UserModel = mongoose.model("UserModel", UserSchema);

/**
 * schema for creating TimeSheetModel
 */
const TimesheetsSchema = new mongoose.Schema({
  hours: 									{ type: Number, 	required: true },
  description: 						{ type: String, 	required: true },
  date: 									{ type: String, 	required: true },
  emp_id: 								{ type: Number, 	required: true } // this is a primary key reference TODO
});
const TimeSheetModel = mongoose.model("TimesheetModel", TimesheetsSchema);

//exporting models
export { UserModel, TimeSheetModel };
