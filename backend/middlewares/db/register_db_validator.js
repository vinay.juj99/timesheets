//importing internal dependencies
// importing Employee_Data model
import {UserModel} from '../../models/models.js'

/**
 * register_db_handler() is used for unique email, emp_id, username, mobile_no
 * @param {*} req email, emp_id, aadhar_no as body params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 * @param {*} next it is middleware if all cteria of this function is met it will allows to go for next function
 */

function register_db_validator(req, res, next) {
  const { email, emp_id, username, mobile_no } = req.body;

  UserModel.find({
    $or: [
      { email },
      { emp_id },
      { username },
      { mobile_no }
    ]}, (err, dataArray) => {
      if (err) {
        res.status(500).send("Database err " + err)
      } 
      else {
        if (dataArray.length != 0) {   
          const returnArray = []; 
          dataArray.forEach(user_found => {
            
            if (user_found.email == email) {
              returnArray.push(email + " email already exists");
            } 
            if (user_found.emp_id == emp_id) {
              returnArray.push(emp_id + " emp_id already exists");
            } 
            if (user_found.username == username) {
              returnArray.push(username + " username already exists");
            } 
            if (user_found.mobile_no == mobile_no) {
              returnArray.push(mobile_no + " mobile already exists");
            } 
          });
          res.status(409).send({ err: returnArray });
          return;
        } else {
          // there are no matches, we are good to go
          next();
        }
      }
  });
}

//exporting function
export { register_db_validator };
