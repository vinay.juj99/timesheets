import {
  _emp_id_checker,
  _otp_checker,
  _password_checker,
  _password_reset_secret_checker,
  _return_object_keys,
  _security_answer_checker,
} from "./helper_functions.js";

/**
 * This is a helper middleware function to help with validation of register
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 */
const forgot_validator = (req, res, next) => {

  const { emp_id, password, otp } = req.body;

  const error_object = {
    ..._emp_id_checker(emp_id),
    ..._password_checker(password),
    ..._otp_checker(otp)
  };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send(error_object);
  } else {
    next();
  }
};

export { forgot_validator };
