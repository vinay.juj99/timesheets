import { _emp_id_checker, _return_object_keys } from "./helper_functions.js";

/**
 * This is the delete_validator to validate the QUERY params 
 * sent in this DELETE call
 */
const delete_validator = (req, res, next) => {

  const { emp_id } = req.params; 
	const emp_id_as_number = parseInt(emp_id);
  // check if everything is okay with all params
	if (isNaN(parseInt(emp_id))) {
		// passed parameter cannot be made a number
		res.status(401).send({ err: "emp_id in query should be a number " + emp_id })
	} else {
		const error_object = { ..._emp_id_checker(emp_id_as_number)};

		if (_return_object_keys(error_object).length > 0) {
			res.status(400).send({err: error_object});
		} else {
			next();
		}
	}
};
export { delete_validator };