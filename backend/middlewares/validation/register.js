import {
  _email_checker,
  _emp_id_checker,
  _mobile_no_checker,
  _password_checker,
  _password_reset_secret_checker,
  _return_object_keys,
  _role_checker,
  _security_answer_checker,
  _username_checker,
} from "./helper_functions.js";

/**
 * This is the register_handler function to handle the parameter validations
 * for the '/register' call.
 */
const register_validator = (req, res, next) => {
  // get all parameters required for the register call emp_id, username, password, email, role, mobile_no
  const {
    emp_id,
    username,
    password,
    email,
    role,
    mobile_no,
    password_reset_secret,
	  security_answer
  } = req.body;
  // check if everything is okay with all params
  const error_object = {
    ..._emp_id_checker(emp_id),
    ..._username_checker(username),
    ..._password_checker(password),
    ..._email_checker(email),
    ..._role_checker(role),
    ..._mobile_no_checker(mobile_no),
    ..._password_reset_secret_checker(password_reset_secret),
	  ..._security_answer_checker(security_answer)
  };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send({err: error_object});
  } else {
    next();
  }
};

export { register_validator };
