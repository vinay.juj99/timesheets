// importing internal dependencies
import { _emp_id_checker, _return_object_keys, _security_answer_checker } from "./helper_functions.js";

/**
 * This is the forgot_handler function to handler the parameter validations
 * for the '/login' call.
 */
const forgot_password_otp_validator = (req, res, next) => {
  // get all parameters required for the login call emp_id, password
  const { emp_id, security_answer } = req.body;
  // check if everything is okay with all params
  const error_object = { ..._emp_id_checker(emp_id), ..._security_answer_checker(security_answer) };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send({err: error_object});
  } else {
    next();
  }
};
export { forgot_password_otp_validator };
