import {
  _hours_checker,
  _description_checker,
  _date_checker,
  _return_object_keys,
} from "./helper_functions.js";

/**
 * This is the timesheet_validator function to handle the parameter validations
 * for the '/timesheet' call.
 */
const timesheet_validator = (req, res, next) => {
  // get all parameters required for the register call emp_id, username, password, email, role, mobile_no
  const { hours, description, date } = req.body;
  // check if everything is okay with all params
  const error_object = {
    ..._hours_checker(hours),
    ..._description_checker(description),
    ..._date_checker(date),
  };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send({err: error_object});
  } else {
    next();
  }
};

export { timesheet_validator };
