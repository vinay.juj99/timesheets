import { _emp_id_checker, _password_checker, _return_object_keys } from "./helper_functions.js";

/**
 * This is the login_handler function to handler the parameter validations
 * for the '/login' call.
 * @param req this is a parameter to get request from user
 * @param res this is a response parameter
 * @returns error_object
 */
const login_validator = (req, res, next) => {
  const { emp_id, password } = req.body;
  const error_object = {
    ..._emp_id_checker(emp_id),
    ..._password_checker(password),
  };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send({err: error_object});
  } else {
    next();
  }
};

export { login_validator };
