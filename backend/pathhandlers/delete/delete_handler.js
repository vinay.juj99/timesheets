import { UserModel } from "../../models/models.js";

/**
 * This is a  function to soft delete the emp_details
 * @param request express request object
 * @param response express response object
 * @returns An object containing result or error messages.
 */
const delete_handler = (request, response) => {

	let { emp_id } = request.params;
	emp_id = parseInt(emp_id);

	UserModel.findOne({ emp_id }, (err, dataObj) => {
		if (err) {
			response.status(500).send({ err: "Database error. Unable to find user"});
		} else {
			if (dataObj == null) {
				response.status(404).send({ err: "No user with emp_id " + emp_id + " found." });
			} else {
				if (!dataObj.is_active) {
					response.status(201).send({ err: "User is already deleted" });
				} else {
					// check if deleting self
					if (emp_id == request.emp_id) {
						response.status(201).send({ err: "Cannot delete yourself!" })
					} else {
						dataObj.is_active = false;
						dataObj.save((err) => {
							if (err) {
								response.status(500).send({ err: "Database error. Unable to delete emp_id " + emp_id })
							} else {
								response.send({ msg: "User with emp_id " +  emp_id + " succesfully deleted." });
							}
						});
					}
				}
			}
		}
	})
};

export { delete_handler };
