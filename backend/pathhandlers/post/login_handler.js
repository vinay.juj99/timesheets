// import bcrypt
import bcrypt from "bcrypt";

// import Employee data model
import { UserModel } from "../../models/models.js";

// importing jwt_token_genrator
import { jwt_token_generator } from "../../middlewares/jwt/jwttoken_helper.js";
import { comparePasswords } from "../../middlewares/hashing/bcrypt_helper.js";

/**
 * This function is used to verify whether the user exists or not.
 * If the user exists then checks the log_in params for login purpose
 * @param {*} req email,password from body which are used for user login
 * @param {*} res sends statuscode and status_message as response for successfull login '200'
 *
 */
function login_handler(request, response) {
  // accessing email and pasword from body
  const { emp_id, password } = request.body;

  // Checking whether the employee email exists or not
  UserModel.findOne({ emp_id }, (err, dataObj) => {
    if (err) {
      response.status(406).send({ status_message: "Error in finding user by emp_id" });
    } else {
      if (dataObj == null) {
        response.status(404).send({ status_message: "No user with this " + emp_id + " exists." });
      } else {

				if (!dataObj.is_active) { // check if user is not active
					response.status(403).send({ err: "This user cannot login, is inactive." });
				} else {
					const hashed_password = dataObj.password;
					comparePasswords(password, hashed_password, (err, res) => {
						if (err) {
							// unable to match password, some bcrypt error
								response.status(404).send({ err: "Unknown error " + err });
						} else {
							if (!res) {
								response.status(404).send({ err: "Username and password do not match" });
							} else { // passwords matched
								const jwt_token = jwt_token_generator(dataObj.emp_id, dataObj.role)
								response.send({ msg: "Login successful", jwt_token })
							}
						}
					});
				}
      }
    }
  });
}

export { login_handler };
