import { TimeSheetModel, UserModel } from "../../models/models.js";

/**
 * This function is used to verify whether the user exists or not.
 * If the user exists then checks the log_in params for login purpose
 * @param {*} req email,password from body which are used for user login
 * @param {*} res sends statuscode and status_message as response for successfull login '200'
 *
 */
const time_sheet_handler = (request, response) => {
  const { date, hours, description } = request.body;
  const { emp_id } = request
  const timesheet_data = { date, emp_id, hours, description };
  // Checking whether the employee email exists or not
  UserModel.findOne({ emp_id }, (err, dataObject) => {
    if (err) {
      response.status(406).send({ err: "Error in finding user by emp_id" });
    } else {
      if (dataObject == null) {
        response.status(401).send({ err: "Unable to find employee by emp_id " + emp_id});
      } else {

        // check if timesheet is over 9 hours TODO later

        const newTimeSheetObject = new TimeSheetModel(timesheet_data);
        newTimeSheetObject.save((err) => {
          if (err) {
            response.status(500).send({ err: "Unable to create new timesheet entry " + err});
          } else {
            response.send({ msg: "Timesheet succesfully added" });
          }
        });
      }
    }
  });
}

export { time_sheet_handler };
