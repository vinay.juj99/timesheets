//import User data...
import { UserModel } from "../../models/models.js";
import { getEncryptedPassword } from "../../middlewares/hashing/bcrypt_helper.js";

/**
 * This register_handler function will save the register details
 * @param {*} request
 * @param {*} response
 * @param {*} next
 */
function register_handler(request, response) {
  const {
    emp_id,
    username,
    password,
    email,
    role,
    mobile_no,
    security_answer,
    password_reset_secret,
  } = request.body;

  const user_data = {
    emp_id,
    username,
    password,
    email,
    role,
    mobile_no,
    security_answer,
    password_reset_secret,
  };

 getEncryptedPassword(user_data.password, (err, hashed_password) => {
  user_data.password = hashed_password;
  const newUser = new UserModel(user_data);

  newUser.save((err, saved_object) => {
    if (err) {
      response.status(500).send("Unable to create user, server error! " + err);
    } else {
      response.send({ msg: "User registered successfully"})
    }
  });
 });
  
}

export { register_handler };
