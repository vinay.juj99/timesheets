// DELETE
export { delete_handler } from "./delete/delete_handler.js";

// GET
export { timesheet_details_handler } from "./get/timesheet_details_handler.js";
export { empdetails_handler } from "./get/empdetails_handler.js";

// POST
export { login_handler } from "./post/login_handler.js";
export { register_handler } from "./post/register_handler.js";
export { time_sheet_handler } from "./post/time_sheet_handler.js";

// PUT
export { forgot_handler } from "./put/forgot_handler.js";
export { forgot_password_handler } from "./put/forgot_password_handler.js";