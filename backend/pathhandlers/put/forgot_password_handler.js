import {UserModel} from '../../models/models.js';
import { getEncryptedPassword } from '../../middlewares/hashing/bcrypt_helper.js';


function forgot_password_handler(req, res, next) {
  // { emp_id, otp, password}
  const { emp_id, otp, password } = req.body;

  UserModel.findOne({
    $and: [
      { emp_id },
      { otp }
    ]}, (err, dataObj) => {
      if (err) {
        res.status(500).send({ err: "Database error. Unable to find user with otp " + err});
      } else {
        if (dataObj == null) {
          res.status(401).send({ err: "Emp_id and OTP do not match"});
        } else { // emp_id and otp match
					getEncryptedPassword(password, (err, hashed_password) => {
						dataObj.password = hashed_password;
					
						dataObj.save((err, saved_object) => {
							if (err) {
								res.status(500).send({ err: "Unable to update password now " + err});
							} else {
								res.send({ msg: "Password reset succesfully!"})
							}
						});
					 });
        }
      }
    })
}

export { forgot_password_handler };
