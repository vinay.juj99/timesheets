// import Employee data model
import { UserModel } from "../../models/models.js";

import { _generateOTP } from "../../middlewares/otp/otp_helper.js";

/**
 * This function is used to verify whether the user exists or not.
 * If the user exists then checks the log_in params for login purpose

 * @param {*} req email,password from body which are used for user login 
 * @param {*} res sends statuscode and status_message as response for successfull login '200' 
 * 
 */
async function forgot_handler(request, response) {
  // accessing email and pasword from body
  const { emp_id, security_answer } = request.body;

	UserModel.findOne({ emp_id }, (err, dataObject) => {

		if (err) {
			response.status(500).send({ err: "Database error in finding user by emp_id"});
		} else {
			if (dataObject == null) {
				response.status(404).send({ err: "No user with this emp_id " + emp_id  + " exists."})
			} else {
				if (security_answer.toLowerCase().trim() != dataObject.security_answer.toLowerCase().trim()) {
					response.status(404).send({ err: "Security answer " + security_answer + " does not match database."})
				} else {
					const otp = _generateOTP();
          if (!otp) {
						response.status(401).send({ err: "Unable to generate OTP, please try later. Or contact admin." })
          } else {
						dataObject.otp = otp;
						dataObject.save((err) => {
							if (err) {
								response.status(500).send({ err: "Unable to update database with new OTP" })
							} else {
								response.status(200).send({ otp: otp });
							}
						});
					}
				}
			}
		}
	});
}

export { forgot_handler };
