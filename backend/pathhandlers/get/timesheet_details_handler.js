import { TimeSheetModel } from "../../models/models.js";

/**
 * This is a  function to get timesheet details
 * @param req this is the parameter request from the body
 * @param res this is the parameter
 * @returns An object containing result or error messages.
 */
const timesheet_details_handler = (req, res) => {

  const { emp_id } = req;
  
  TimeSheetModel.find({ emp_id }, "hours description date -_id", (err, dataArray) => {
    if (err) {
      res.status(500).send({ err: "Database error " + err });
    } else {
      const returnArray = [];
      dataArray.forEach(timeSheetObject => {
        returnArray.push(timeSheetObject);
      });
      res.send({ msg: "Timesheet entries succesfully found", timesheet_details: returnArray });
    }
  });
};

export { timesheet_details_handler };
