import { UserModel } from "../../models/models.js";

/**
 * This is a  function to get the details of the employee profile
 * @param req request object of Express
 * @param res response object of Express
 */
const empdetails_handler = (req, res) => {

  const { emp_id } = req;

  UserModel.findOne({ emp_id }, "emp_id username email role mobile_no -_id", (err, dataObject) => {
    if (err) {
      res.status(500).send({ err: "Database error, unable to find user " + err });
    } else {
      if (dataObject == null) {
        res.status(401).send({ err: "No user found! Strange case!"});
      } else {
        res.send(dataObject);
      }
    }
  });
};

export { empdetails_handler };
