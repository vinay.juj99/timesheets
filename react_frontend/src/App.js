import React from 'react';
import { BrowserRouter, Route, Switch} from "react-router-dom";

import LandingPage from './components/pages/LandingPage';
import Login from './components/pages/LoginPage';

import Hrlogin from "./components/Hrpanel/hrlogin";
import Adminlogin from './components/adminfolder/adminlogin/Alogin';

import Emplandingpage from './components/Emplandingpage/emplandingpage';
import Timesheet from './components/Emplandingpage/timesheet';
import Employeedetails from './components/Emplandingpage/employeedetails';

import Empleaves from './components/Emplandingpage/empleaves';

import Panel from './components/Hrpanel/baner';

import Admindashboard from './components/adminfolder/admindashbd/admindshbd';
import Registration from './components/adminfolder/Registration/Registration';

import Forgot from './components/forgot/forgot';
import Otp from './components/forgot/otp';
import ResetPassword from './components/forgot/reset';
import Delete from './components/delete/delete';
import Empcard from './components/adminfolder/Rempcard/Remplcard';
import Emptasks from './components/Emplandingpage/emptasks';


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/"                 exact component={LandingPage} />
        <Route path="/login"            exact component={Login} />
        <Route path="/adminlogin"       exact component={Adminlogin} />
        <Route path="/empleaves"        exact component={Empleaves} />
        <Route path="/forgot"           exact component={Forgot} />
        <Route path="/emplandingpage"   exact component={Emplandingpage} />
        <Route path="/otp"              exact component={Otp} />
        <Route path="/reset"            exact component={ResetPassword} />
        <Route path="/tasks"            exact component={Emptasks} /> 
        <Route path="/hrlogin"          exact component={Hrlogin} />
        <Route path="/admindashboard"   exact component={Admindashboard} />
        <Route path="/employeedetails"  exact component={Employeedetails} />
        <Route path="/addemps"          exact component={Registration} />
        <Route path="/timesheet"        exact component={Timesheet} />
        <Route path="/hrpanel"          exact component={Panel} />
        <Route path="/employeedetails"  exact component={Employeedetails} />
        <Route path="/admindashboard"   exact component={Admindashboard} />
        <Route path="/empregister"      exact component={Registration} />
        <Route path="/delete"           exact component={Delete} />
        <Route path="/removeempcard"    exact component={Empcard} />
        
      </Switch>
    </BrowserRouter>
  );
}

export default App;
