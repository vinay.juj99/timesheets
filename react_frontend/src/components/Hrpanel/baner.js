import React from 'react';
import './hrpanel.css';
import Employee from './empdata.js';

function Panel() {

    return (

      <div>
        <header role="banner">
          <h1> HR panel</h1>
          <ul class="utilities">
            <br />
            <li class="users"><a href="/#">My Account</a></li>
            <li class="logout warn"><a href="/#">Log Out</a></li>
          </ul>
        </header>

        <div />
        <Employee/>
      </div>

    );
  }



export default Panel;   
