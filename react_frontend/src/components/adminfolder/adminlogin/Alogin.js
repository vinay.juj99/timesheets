import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./adminlogin.css";

class Adminlogin extends Component {
  render() {
    return (
      <div>
      <div className="login">
        <form className="form">
          <h3> LogIn </h3>
          <hr />
          <div className="icontainer">
            <span className="fa fa-user icon" />
            <input type="text" placeholder="Enter Your Emp_id" />
          </div>
          <div className="icontainer">
            <span className="fa fa-lock icon" />
            <span className="fa fa-eye show1" />
            <input
              type="password"
              placeholder="Enter Your password"
              className="cp"
            />
          </div>
          <hr />
          <Link to='/forgot'>
          <p className="text-right">
            <a href="/#">Forgot Password...?</a>
          </p></Link>
          <Link to="/admindashboard">
            <button className="btnedesign">LogIn</button>
          </Link>
        </form>
      </div>
    </div>
     
    );
  }
}

export default Adminlogin;