import React from "react";

import './Rempcard.css';
function Empcard() {
    return (
        <div>
          <title>Employee card</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
          <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossOrigin="anonymous" />
          <div className="container">
            <div className="card">
              <div className="slide slide1">
                <div className="content">
                  <div className="icon">
                    <i className="fa fa-user-circle" aria-hidden="true" />
                  </div>
                </div>
              </div>  
              <div className="slide slide2">
                <div>
                <div className="content">
                  <h3>innostars</h3>
                  <h4>Employee card.</h4>
                  </div>
                  <p style={{margin:"7px"}}>name:Hari</p>
                  <p style={{margin:"7px"}}>empid:10761</p>
                  <p style={{margin:"5px"}}>email:abcd@gmail.com</p>
                  <div>
                  <button>Remove</button> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
     
      )
    }

export default Empcard;




