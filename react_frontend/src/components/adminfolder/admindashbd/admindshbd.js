import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./admindshbd.css";

class Admindashboard extends Component {
  render() {
    return (
			<div>
      <header role="banner">
        <h1>Admin Panel</h1>
        <ul className="utilities">
          <br />
          <li className="users">
            <a href="/#">My Account</a>
          </li>
          <li className="logout warn">
            <a href="/#">Log Out</a>
          </li>
        </ul>
        <h1 align="center">WELCOME</h1>
      </header>
      <nav role="navigation">
        <ul className="main">
          <li className="dashboard">
            <a href="admindashboard">Dashboard</a>
          </li>
          <br />
          <li className="users">
            Employee Details{" "}
              <Link to="/empregister">
                <button>Add Employee</button>
              </Link>
            {" "}
              <Link to="/delete">
                <button>Remove Employee</button>
              </Link>
          </li>
        </ul>
      </nav>
    </div>
      
    );
  }
}

export default Admindashboard;


