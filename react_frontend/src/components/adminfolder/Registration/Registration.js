
import './Registration.css';

import React from 'react';
import { Link } from 'react-router-dom';


class Registration extends React.Component {
   state = {
       emp_id: " ",
       username: " ",
       password: " ",
       email: " ",
       role: " ",
       mobile_no: " ",
       security_answer: " ",
       isSignUp:false,
       error_msg:" "

   }



   signUpSuccess = () => {
       const { history } = this.props
       history.push("./emp_homepage")
   }

   apiCallFail = (data) => {
       this.setState({ isSignUp: true, error_msg: data.status_message })
   }

   signupApiCall = async (event) => {
       event.preventDefault();
       console.log(this.state);

       const { username, password, email, password_reset_secret, role, security_answer, emp_id, mobile_no } = this.state
       const url = "http://localhost:3001/create_profile"
       const userDetails = {
           emp_id,
           username,
           password,
           email,
           password_reset_secret,
           role,
           mobile_no,
           security_answer
       }
       const option = {
           method: "POST",
           body: JSON.stringify(userDetails),
           headers: {
               "Content-Type": "application/json",
               "Accept": "application/json"
           }
       }
       const response = await fetch(url, option)
       const data = await response.json()
       console.log(data);
       if (data.status_code === 200) {
           this.signUpSuccess()
       }
       else {
           this.apiCallFail(data)
       }
   }

   changeUsername = (event) => {
       this.setState({ username: event.target.value })
   }

   ChangeEmpid = (event) => {
       this.setState({ emp_id: event.target.value })
       // var emp_id= parseInt(emp_id)

   }

   changePassword = (event) => {
       this.setState({ password: event.target.value })
   }

   changeEmail = (event) => {
       this.setState({ email: event.target.value })
   }
   changeSecurity = (event) => {
       this.setState({ password_reset_secret: event.target.value })

   }

   changeRole = (event) => {
       this.setState({ role: event.target.value })
   }
   changeMobileno = (event) => {
       this.setState({ mobile_no: event.target.value })
   }
   changeSecurityanswer = (event) => {
       this.setState({ security_answer: event.target.value })
   }
   render() {
       // const {isSignUp,error_msg} = this.state
       return (

           <div class="testbox">
               <h1>Registration</h1>

               <form onSubmit={this.signupApiCall} >

                   <label id="icon" for="empid"><i class="icon-envelope "></i></label>
                   <input type="text" name="name" id="empid" placeholder="Employee Id" required onChange={this.ChangeEmpid} />
                   <label id="icon" for="username"><i class="icon-user"></i></label>
                   <input type="text" name="name" id="username" placeholder="Username" required onChange={this.changeUsername} />
                   <label id="icon" for="password"><i class="icon-shield"></i></label>
                   <input type="text" name="name" id="password" placeholder="Password" required onChange={this.changePassword} />
                   <label id="icon" for="email"><i class="icon-user"></i></label>
                   <input type="text" name="name" id="email" placeholder="Email" required onChange={this.changeEmail} />
                   <label id="icon" for="role"><i class="icon-user"></i></label>
                   <input type="text" name="name" id="role" placeholder="Role" required onChange={this.changeRole} />
                   <label id="icon" for="mobilenumber"><i class="icon-user"></i></label>
                   <input type="tel" name="name" id="mobilenumber" placeholder="Mobile Number" required onChange={this.changeMobileno} />
                   <label id="icon" for="securityquestion"><i class="icon-user"></i></label>
                   <input type="text" name="name" id="securityquestion" placeholder="Security Question" required onChange={this.changeSecurity} />
                   <label id="icon" for="securityanswer"><i class="icon-user"></i></label>
                   <input type="text" name="name" id="securityanswer" placeholder="Security Answer" required onChange={this.changeSecurityanswer} /><br /><br />
                   <p>By clicking Register, you agree on our <a href="/#">terms and condition</a></p>
                   <Link to='/addedsucces'>
                 <button class="btn" type='submit'>Register</button> </Link>
               </form>
           </div>


       );
   }
}
export default Registration;

