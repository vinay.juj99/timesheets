import React from "react";
import { Link } from "react-router-dom";
import './forgot.css';


function Forgot() {
    return (
        <div className="login-page">
            <div className="form">
                <h3>Forgot password</h3>
                <br></br>
                <form className="register-form">
                    <input type="text" placeholder="name" />
                    <input type="password" placeholder="password" />
                    <input type="text" placeholder="email address" />
                    <button>create</button>
                    <p className="message">Already registered? <a href="/#">Sign In</a></p>
                </form>
                <form className="login-form">
                    <input type="text" placeholder="employee id" />
                    <input type="password" placeholder="security question" />
                    <Link to='/otp'>
                    <button>Submit</button></Link>
                    {/* <p className="message">Not registered? <a href="/#">Create an account</a></p> */}
                </form>
            </div>
        </div>
        
        )
    }

export default Forgot;

