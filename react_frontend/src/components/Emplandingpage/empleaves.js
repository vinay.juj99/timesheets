import React, { Component } from "react";
import "./empleaves.css";

class Empleaves extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-md-6 div-employees">
          <label>
            <u>EMPLOYEE SIDE</u>
          </label>
          <br />
          <table className="tbl-employees">
            <colgroup>
              <col />
            </colgroup>
            <colgroup span={2} />
            <thead>
              <tr>
                <th>Employee ID</th>
                <th
                  colSpan={5}
                  scope="colgroup"
                  style={{ textAlign: "center" }}
                >
                  Leaves
                </th>
              </tr>
              <tr>
                <th></th>
                <th scope="col">Sick</th>
                <th scope="col">Vacation</th>
                <th scope="col">Birthday</th>
                <th scope="col">Date</th>
                <th scope="col">Mental health</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <span name="emp-id">IPPH0010</span>
                </td>
                <td>
                  <span name="sick-count">5</span>
                </td>
                <td>
                  <span name="vacation-count">5</span>
                </td>
                <td>
                  <span name="birthday-count">1</span>
                </td>
                <td>
                  <span name="date-count">1</span>
                </td>
                <td>
                  <span name="mental-count">1</span>
                </td>
              </tr>
            </tbody>
          </table>
          <br />
          <br />
          <span className="type-validator">*Please fill up empty fields</span>
          <br />
          <select id="leave_type">
            <option disabled selected>
              SELECT LEAVE
            </option>
            <option value="Sick leave">Sick leave</option>
            <option value="Vacation leave">Vacation leave</option>
            <option value="Birthday leave">Birthday leave</option>
            <option value="Date leave">Date leave</option>
            <option value="Mental health leave">Mental health leave</option>
          </select>
          <input type="date" id="leave_date" />
          <label>
            <input type="radio" name="rdo-leave" defaultValue="WHOLE DAY" />
            Whole day
          </label>
          <label>
            <input type="radio" name="rdo-leave" defaultValue="HALF DAY" />
            Half day
          </label>
          <button id="leave-button">REQUEST FOR LEAVE</button>
          <br />
          <br />
          <span id="leave-span">
            Your leave request was <span id="result" />.
          </span>
        </div>
        <div className="col-md-6 div-computation">
          <label>
            <u>LEAVE SUMMARY</u>{" "}
          </label>
          <table id="tbl-leaves" className="tbl-leaves">
            <thead>
              <tr>
                <th>Filed by</th>
                <th>Leave type</th>
                <th>Leave date</th>
                <th>Submitted on</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          <br />
          <hr />
          <br />
          <table className="emp_table">
            <thead>
              <tr>
                <td>Employee ID</td>
                <td>First Name</td>
                <td>Middle Name</td>
                <td>Last Name</td>
                <td>Hourly Rate</td>
                <td />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <span id="span-id">IPPH0010</span>
                </td>
                <td>
                  <span id="fname-id">Kate Janeen</span>
                </td>
                <td>
                  <span id="mname-id">Ilag</span>
                </td>
                <td>
                  <span id="lname-id">Martin</span>
                </td>
                <td>
                  <span id="span-rate">0.00</span>
                  <input
                    type="number"
                    className="input_rate"
                    placeholder="Rate"
                  />
                </td>
                <td>
                  <button id="btn-edit">EDIT</button>
                  <button id="btn-save">SAVE</button>
                </td>
              </tr>
            </tbody>
          </table>
          <table className></table>
        </div>
      </div>
    );
  }
}

export default Empleaves;
