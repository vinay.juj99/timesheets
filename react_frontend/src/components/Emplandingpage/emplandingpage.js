import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./emplandingpage.css";

class Emplandingpage extends Component {
  render() {
    return (
      <div>
        <nav className="navbar1">
          {/* LOGO */}
          <div className="logo">
            <img src="https://www.pngall.com/wp-content/uploads/5/User-Profile-PNG-High-Quality-Image.png" alt=""/>
          </div>
          {/* NAVIGATION MENU */}
          <ul className="nav-links">
            {/* USING CHECKBOX HACK */}
            <input type="checkbox" id="checkbox_toggle" />
            <label htmlFor="checkbox_toggle" className="hamburger">
              ☰
            </label>
            {/* NAVIGATION MENUS */}
            <div className="menu">
              <li>
                <a href="/">Home</a>
              </li>
              <li className="services">
                <Link to="/timesheet">
                  <a href="/">Timesheets</a>
                </Link>
              </li>
              <li>
                <Link to="/empleaves">
                  <a href="/#">MyLeaves</a>
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/emplogin">
                  <a href="/#">Logout</a>
                </Link>
              </li>
            </div>
          </ul>
        </nav>
        <div className="container2 h-100">
          <div className="row h-100 align-items-center justify-content-center">
            <div className="main">
              <div className="info">
                <img
                  src="https://www.kindpng.com/picc/m/24-248729_stockvader-predicted-adig-user-profile-image-png-transparent.png" alt=""
                  className="prof rounded-circle img-fluid"
                />
                <hr />
              </div>
              <div className="body">
                <ul className="fa-ul">
                  <li>
                    <h2>Name:Jhon</h2>
                  </li>
                  <li>
                    <h2>Employee id:10777</h2>
                  </li>
                  <li>
                    <h2>Role:HR</h2>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
     
    );
  }
}

export default Emplandingpage;
