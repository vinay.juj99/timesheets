import React from "react";
import "./emptasks.css";

class Emptasks extends React.Component {
state ={
   emp_id:0,
   date:"",
   hours:0,
   description:" "
}
submitSuccess = () => {
    const { history } = this.props
    history.push("./timesheet")
}
apiCallFail = (data) => {
    this.setState({ isSignUp: true, error_msg: data.status_message })
}
submitApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);
    const { emp_id, date, hours, description } = this.state
       const url = "http://localhost:3001/timesheet"
       const timesheetDetails = {
           emp_id:parseInt(emp_id),
           hours,
           date,
           description
        }
        const option = {
            method: "POST",
            body: JSON.stringify(timesheetDetails),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }
        const response = await fetch(url, option)
        const data = await response.json()
        console.log(data);
        if (data.status_code === 200) {
            this.submitSuccess()
        }
        else {
            this.apiCallFail(data)
        }
    }
    ChangeEmpid = (event) => {
        this.setState({ emp_id: event.target.value })
        // var emp_id= parseInt(emp_id)
 
    }
 
    ChangeDate = (event) => {
        this.setState({date: event.target.value })
    }
 
    ChangeHours= (event) => {
        this.setState({ hours: event.target.value })
    }
    ChangeDescription = (event) => {
        this.setState({ description: event.target.value })
 }
 
 render() {
      return (
      <form onSubmit={this.submitApiCall}>
        
        <div className="container">
          <label htmlFor="uname">
            <b>Emp ID</b>
          </label>
          <input type="text" placeholder="Enter Emp ID" name="uname" required onChange={this.ChangeEmpid}/>
          <label htmlFor="date" className="dat">
            <b>Date:</b>
          </label>
          <br />
          <input type="date" id="date" name="birthday" required onChange={this.ChangeDate}/>
          <br />
          <label htmlFor="uname" className="dat">
            <b>Enter Hours</b>
          </label>
          <input type="text" placeholder="Enter Hours" name="uname" required onChange={this.ChangeHours}/>
          <label htmlFor="psw">
            <b>Tasks Done</b>
          </label>
          <input
            type="text"
            placeholder="Enter Your Tasks or description"
            name="psw"
            required
            onChange={this.ChangeDescription}/>
          <button type="submit">Submit</button>
        </div>
        <div className="container" style={{ backgroundColor: "#f1f1f1" }}>
          <button type="button" className="cancelbtn">
            Cancel
          </button>
        </div>
      </form>
    );
  }
}

export default Emptasks;
