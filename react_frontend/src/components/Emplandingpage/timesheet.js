
import React from 'react';
import { Link } from 'react-router-dom';
import './timesheet.css';
class Timesheet extends React.Component {

	render() {
		return (

			<div classNameName="d-flex flex-column">
				<div classNameName="d-flex flex-row">
					<div classNameName="header1 m-2" ><h4>Work from home</h4></div>
					<div classNameName="header2 m-2"><h4>Holidays</h4></div>
					<div classNameName="header3 m-2"><h4>Leave</h4></div>
					<div classNameName="header4 m-2"><h4>Present days</h4></div>
				</div>
				<h4>Feb 2022</h4>
				<div>
                    <table style={{width: '100%'}}>
						<tr>
							<th>Monday</th>
							<th>Tuesday</th>
							<th>Wednesday</th>
							<th>Thursday</th>
							<th>Friday</th>
							<th>Saturday</th>
							<th>Sunday</th>
						</tr>
						<tr>

						<td><Link to='/tasks'><button className="btn3"></button></Link>1</td>
							<td><button className="btn3"></button>2</td>
							<td><button className="btn3"></button>3</td>
							<td><button className="btn3"></button>4</td>
							<td><button className="btn3"></button>5</td>
							<td><button className="btn3"></button>6</td>
							<td><button className="btn3"></button>7</td>

						</tr>
						<tr>
							<td><button className="btn3"></button>8</td>
							<td><button className="btn3"></button>9</td>
							<td><button className="btn3"></button>10</td>
							<td><button className="btn3"></button>11</td>
							<td><button className="btn3"></button>12</td>
							<td><button className="btn3"></button>13</td>
							<td><button className="btn3"></button>14</td>
						</tr>
						<tr>
							<td><button className="btn3"></button>15</td>
							<td><button className="btn3"></button>16</td>
							<td><button className="btn3"></button>17</td>
							<td><button className="btn3"></button>18</td>
							<td><button className="btn3"></button>19</td>
							<td><button className="btn3"></button>20</td>
							<td><button className="btn3"></button>21</td>
						</tr>
						<tr>
							<td><button className="btn3"></button>22</td>
							<td><button className="btn3"></button>23</td>
							<td><button className="btn3"></button>24</td>
							<td><button className="btn3"></button>25</td>
							<td><button className="btn3"></button>26</td>
							<td><button className="btn3"></button>27</td>
							<td><button className="btn3"></button>28</td>
						</tr>
						<tr>
							<td><button className="btn3"></button>29</td>
							<td><button className="btn3"></button>30</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
		);
	}
}


export default Timesheet;  
