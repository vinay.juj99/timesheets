import React from 'react';

import './employeedetails.css';
class Employeedetails extends React.Component {

    render() {
        return (
            <main role="main">
                <section>
                    <h1>Employee Details</h1>
                    <br />
                    <div class="tbl-header">
                        <table cellpadding="10" cellspacing="10" border="0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Emp_Id</th>
                                    <th>Emp_name</th>
                                    <th>E_Timesheet</th>
                                    <th>Emp_salary</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tbl-content">
                        <table cellpadding="10" cellspacing="10" border="0">
                            <body>

                                <tr>
                                    <td>1</td>
                                    <td>1077</td>
                                    <td>Anusha.P</td>
                                    <td>abcd</td>
                                    <td>20000</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>1068</td>
                                    <td>Vinay.J</td>
                                    <td>efgh</td>
                                    <td>20000</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>1055</td>
                                    <td>Hemanthkumar.k</td>
                                    <td>India</td>
                                    <td>20000</td>
                                </tr>
                            </body>
                        </table>
                    </div>
                </section>
                <button><input type="button" value="Download" class="btn" /></button>
            </main>
        );
    }
}
export default Employeedetails;
