import React from "react";
import { Link } from "react-router-dom";

import "./index.css";

function LandingPage() {
  return (
    <div>
    <nav class="navbar">
      {/* <!-- LOGO --> */}
      <div class="logo">TimeSheet</div>
      {/* <!-- NAVIGATION MENU --> */}
      <ul class="nav-links">
        {/* <!-- USING CHECKBOX HACK --> */}
        <input type="checkbox" id="checkbox_toggle" />
        <label for="checkbox_toggle" class="hamburger">
          &#9776;
        </label>
        {/* <!-- NAVIGATION MENUS --> */}
        <div class="menu">
          <li>
            <a href="/">Home</a>
          </li>
          <li class="services">
            <Link to="/login">
              <a href="/">Login</a>
            </Link>  
          </li>
          <li>
            <a href="/#">Contact</a>
          </li>
        </div>
      </ul>
    </nav>
    <div className='middle'>
   <h1>Welcome</h1>
   <div ><img src="https://innostars.uschinainnovation.org/wp-content/uploads/2017/11/innoSTARS-logo-1024x245.png" style={{width:"40%"}} className="profile" alt=""/></div>
   <h3>"Believe in your infinite potential."</h3>
   </div>
   </div>
  );
}

export default LandingPage;
