import React, { Component } from "react";
import { Link } from "react-router-dom";
import { _emp_id_checker, _password_checker, _return_object_keys } from "../../../validators/helper_functions";
import "./index.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emp_id: "",
      password: "",
      emp_id_err: "",
      password_err: "",
      login : false
    };

    this.handleEmpId = this.handleEmpId.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.validateEmpID = this.validateEmpID.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
    this.loginCall = this.loginCall.bind(this);
  }

  // handle changes to emp_id input in JSX
  handleEmpId = (event) => {
    let newEmpID = event.target.value;
    newEmpID = newEmpID.replace(/\D/g, "");
    this.setState({ emp_id: newEmpID, emp_id_err: "" });
  };
  // handle on blur to emp_id input in JSX
  validateEmpID = () => {
    const emp_id = this.state.emp_id;
    const emp_id_errors = _emp_id_checker(parseInt(emp_id));
    const is_emp_id_validated = _return_object_keys(emp_id_errors).length === 0;
    console.log(emp_id_errors);
    console.log(is_emp_id_validated);
    if (!is_emp_id_validated) {
      // emp_id validation failed
      this.setState({ emp_id_err: emp_id_errors.emp_id });
    }
  };

  // handle changes to password input in JSX
  handlePassword = (event) => {
    const newPassword = event.target.value;
    this.setState({ password: newPassword, password_err: "" });
  };
  // handle onBlur to password in JSX
  validatePassword = () => {
    const password = this.state.password;
    const password_errors = _password_checker(password);
    const is_password_validated =
      _return_object_keys(password_errors).length === 0;
    console.log(password_errors);
    console.log(is_password_validated);
    if (!is_password_validated) {
      // emp_id validation failed
      this.setState({ password_err: password_errors.password });
    }
  };
  loginInSuccess = () => {
    const { history } = this.props;
    history.push("./admindashboard");
  };
  loginCall = () => {
    let errors_exist = false;
    if (
      !_return_object_keys(_emp_id_checker(parseInt(this.state.emp_id)))
        .length === 0
    ) {
      // emp_id incorrect
      errors_exist = true;
      this.validateEmpID();
    }
    if (
      !_return_object_keys(_password_checker(this.state.password)).length === 0
    ) {
      // password incorrect
      errors_exist = true;
      this.validatePassword();
    }

    if (errors_exist) {
      return;
    } else {
      console.log("hello");
      // return;

      const { emp_id, password } = this.state;
      const userdetails = {
        emp_id: parseInt(emp_id),
        password
      };
      const requestOptions = {
        method: "POST",
        body: JSON.stringify(userdetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      fetch("http://localhost:3001/login", requestOptions)
        .then((response) => response.json()
        .then((data) =>{
          console.warn("result", data)
          localStorage.setItem('login', JSON.stringify({
            login : true,
            jwt_token : data.jwt_token
          }))
        }));
    }
  };

  render() {
    return (
      <div>
        <div className="login">
          <form className="form">
            <h3> LogIn </h3>
            <hr />
            <div className="icontainer">
              <span className="fa fa-user icon" />
              <input
                type="number"
                placeholder="Employee ID here.."
                name="emp_id"
                onChange={this.handleEmpId}
                onBlur={this.validateEmpID}
                value={this.state.emp_id}
              />
              <p style={{ color: "red" }}>{this.state.emp_id_err}</p>
            </div>
            <div className="icontainer">
              <span className="fa fa-lock icon" />
              <span className="fa fa-eye show1" />
              <input
                type="password"
                placeholder="Your Password here.."
                className="cp"
                name="password"
                onChange={this.handlePassword}
                onBlur={this.validatePassword}
                value={this.state.password}
              />
              <p style={{ color: "red" }}>{this.state.password_err}</p>
            </div>
            <hr />
            <Link to="/forgot">
              <p className="text-right">
                <a href="/#">Forgot Password...?</a>
              </p>
            </Link>
            <button className="btnedesign" onClick={this.loginCall}>
              LogIn
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;