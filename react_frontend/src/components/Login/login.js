import React, { Component } from "react";
import "./login.css";
import { Link } from "react-router-dom";
// import { backend_URL } from "../../constants";
// import Cookies from "js-cookie";

class Signin extends Component {
  state = {
    emp_id: "",
    password: "",
    showSubmitError: false,
    errorMsg: "",
  };

  onChangeemp_id = (event) => {
    this.setState({ emp_id: event.target.value });
  };

  onChangePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  onSubmitFailure = (data) => {
    this.setState({ showSubmitError: true, errorMsg: data.status_message });
  };

  //giving backend url to connect the backend data or to view the data for user

  submitForm = async (event) => {
    event.preventDefault();
    const { emp_id, password } = this.state;
    const userDetails = { emp_id, password };
    const url = "http://localhost:3002/login";
    const options = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options);
    const data = await response.json();
    if (data.status_code !== 200) {
      this.onSubmitFailure(data);
    } else {
      this.onSubmitSuccess(data.jwt_token);
    }
  };

  renderPasswordField = () => {
    const { password } = this.state;
    return (
      <>
        <label className="input-label" htmlFor="password">
          PASSWORD
        </label>
        <input
          type="password"
          id="password"
          className="password-input-field"
          value={password}
          onChange={this.onChangePassword}
          placeholder="Password"
        />
      </>
    );
  };
  render() {
    const { password, emp_id } = this.state;
    return (
      <div className="login_container">
        <div className="login_form_container">
          <div className="left">
            <form className="form_container_login">
              <h1>Login</h1>
              <input
                type="number"
                id="emp_id"
                className="input"
                value={emp_id}
                onChange={this.onChangeemp_id}
                placeholder="emp_id"
              />
              <input
                type="password"
                id="password"
                className="input"
                value={password}
                onChange={this.onChangePassword}
                placeholder="Password"
              />
              {/* {error && <div className="error_msg">{error}</div>} */}
               <Link to='/timesheets'> 
              <button
                onClick={this.submitForm}
                type="submit" className="green_btn">
                Sign In
              </button>
               </Link> 
            </form>
          </div>
          <div className="right">
            <button type="button" className="forgot_button">
              Forgot Password
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Signin;
