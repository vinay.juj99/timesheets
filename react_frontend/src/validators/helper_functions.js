const ALLOWED_ROLES=["hr", "employee", "admin"];
const ALLOWED_HOURS=[1, 2, 3, 4, 5, 6, 7, 8, 9];

/**
 * This is a helper function to check and validate the emp_id
 * @param {} emp_id this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _emp_id_checker(emp_id) {
  const returnObject = {};

  // check if its not sent
  if (emp_id === undefined) {
    returnObject.emp_id = "Parameter emp_id is not sent";
    return returnObject;
  }
  // check if it is numeric type
  if (typeof emp_id !== "number") {
    returnObject.emp_id = 'Parameter "' + emp_id + '" is not a number type';
    return returnObject;
  }
  // number should not be decimal, length should be 5, should be positive
  if (!Number.isInteger(emp_id) || emp_id.toString().length !== 5 || emp_id <= 0) {
    returnObject.emp_id = 'Parameter "' + emp_id + '" is not a valid format';
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate OTP
 * @param {} otp this is the otp param to be validated
 * @returns An object containing error messages.
 */
 function _otp_checker(otp) {
  const returnObject = {};

  // check if its not sent
  if (otp === undefined) {
    returnObject.otp = "Parameter otp is not sent";
    return returnObject;
  }
  // check if it is numeric type
  if (typeof otp !== "number") {
    returnObject.otp = 'Parameter otp: "' + otp + '" is not a number type';
    return returnObject;
  }
  // number should not be decimal, length should be 5, should be positive
  if (!Number.isInteger(otp) || otp.toString().length !== 4 || otp <= 0) {
    returnObject.otp = 'Parameter otp: "' + otp + '" is not a valid format';
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the password
 * @param {} password this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _password_checker(password) {
  const returnObject = {};

  // check if its not sent
  if (password === undefined) {
    returnObject.password = "Parameter password is not sent";
    return returnObject;
  }
  // check if it is string type
  if (typeof password !== "string") {
    returnObject.password = 'Parameter "' + password + '" is not a string type';
    return returnObject;
  }
  // pasword should be alphanumeric and should have at least 1 alphabet, 1 digit, length between 8-20
  if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/.test(password)) {
    returnObject.password = 'Parameter "' + password + '" is not a valid format. pasword should be alphanumeric and should have at least 1 alphabet caps, 1 alphabet in small case, 1 digit, length between 8-20';
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the username
 * @param {} username this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _username_checker(username) {
  const returnObject = {};
  if (username === undefined) {
    returnObject.username = "Username not given";
    return returnObject;
  }
  if (typeof username !== "string") {
    returnObject.username = "Username must be a string";
    return returnObject;
  }
  if (username.length <= 8 && username.length > 6) {
    returnObject.username =
      "Username length must be in between 6 to 8 characters";
  }
}

/**
 * This is a helper function to check and validate the email
 * @param {} email this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _email_checker(email) {
  const returnObject = {};
  //   /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (email === undefined) {
    returnObject.email = "email not given";
    return returnObject;
  }
  if (typeof email !== "string") {
    returnObject.email = "email must be a string";
    return returnObject;
  }
  if (!email.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
    returnObject.email = "email is supposed to be in valid format";
    return returnObject;
  }
  if (!email.endsWith("@gmail.com")) {
    returnObject.email = "Enter a valid mail that ends with @gmail.com";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the role
 * @param {} role this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _role_checker(role) {
  const returnObject = {};
  if (role === undefined) {
    returnObject.role = "role is not mentioned";
    return returnObject;
  }
  if (typeof role !== "string") {
    returnObject.role = "role must be a string";
    return returnObject;
  }
  if (!ALLOWED_ROLES.includes(role)) {
    returnObject.role = "role must be in " + ALLOWED_ROLES;
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the mobile_no
 * @param {} mobile_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _mobile_no_checker(mobile_no) {
  const returnObject = {};
  if (mobile_no === undefined) {
    returnObject.mobile_no = "Mobile Number Not given";
    return returnObject;
  }
  if (typeof mobile_no !== "number") {
    returnObject.mobile_no = "Mobile Number should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(mobile_no)) {
    returnObject.mobile_no = "Mobile Number have no special characters points " + mobile_no;
    return returnObject;
  }
  if (mobile_no.toString().length !== 10) {
    returnObject.mobile_no = "Mobile number should have 10 digits: " + mobile_no;
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the password_reset_secret
 * @param {} password_reset_secret this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _password_reset_secret_checker(password_reset_secret) {
  const returnObject = {};
  if (password_reset_secret === undefined) {
    returnObject.password_reset_secret = "Please give Your password reset secret answer";
    return returnObject;
  }
  if (typeof password_reset_secret !== "string") {
    returnObject.password_reset_secret ="password_reset_secret must be a string";
    return returnObject;
  }
  if (password_reset_secret.length === 0) {
    returnObject.password_reset_secret ="password_reset_secret cannot be empty string";
    return returnObject;
  }

  return returnObject;
}

/*
 * Helper function to validate security_answer string
 * checks if security_question is string, else returns "security_answer should be of string"
 * @param {}security_answer
 * @returns error object
 */
const _security_answer_checker = (security_answer) => {
  const returnObject = {};
  if (security_answer === undefined) {
    returnObject.security_answer = "security_answer should be given";
    return returnObject;
  }
  if (typeof security_answer !== "string") {
    returnObject.security_answer = "security_answer should be of string type";
    return returnObject;
  }
  if (security_answer.length === 0) {
    returnObject.security_answer = "scurity_answer cannot be an empty string";
    return returnObject;
  }

  return returnObject;
};

/**
 * This is a helper function to check and validate the hours
 * @param {} hours this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _hours_checker(hours){
  const returnObject = {};
  // is it a string
  if (hours === undefined) {
    returnObject.hours = "hours is a mandatory parameter";
    return returnObject;
  }
  if (typeof hours !== "number") { 
    returnObject.hours = "hours should be of numeric type";
    return returnObject;
  }  
  if (!Number.isInteger(hours)) {
    returnObject.hours = "Hours has to be integer type";
    return returnObject;
  }
  if (!ALLOWED_HOURS.includes(hours)) { 
    returnObject.hours = "Hours can only be only in " + ALLOWED_HOURS;
    return returnObject;
  }  

  return returnObject;
}

/**
 * This is a helper function to check and validate the description
 * @param {} description this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _description_checker(description) {
  const returnObject = {};
  // is it a string
  if (description === undefined) {
    returnObject.description = "Please give Your description";
    return returnObject;
  }
  if (typeof description !== "string") {
    returnObject.description = "description should be of string type";
    return returnObject;
  }
  if (description.length === 0) {
    returnObject.description = "description cannot be an empty string";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the date
 * @param {} date this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _date_checker(date){
  const returnObject = {};
  // is it a string
  if (date === undefined) {
    returnObject.date = "Please give Your date";
    return returnObject;
  }
  if (typeof date !== "string") {
    returnObject.date = "date should be of string type";
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {
  
  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }

  return returnArray;
}


export {
  _emp_id_checker,
  _password_checker,
  _username_checker,
  _email_checker,
  _role_checker,
  _mobile_no_checker,
  _password_reset_secret_checker,
  _security_answer_checker,
  _hours_checker,
  _description_checker,
  _date_checker,
  _return_object_keys,
  _otp_checker
};
